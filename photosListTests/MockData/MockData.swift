//
//  MockData.swift
//  photosListTests
//
//  Created by Mohamed Hassan Nawar on 18/05/2022.
//

import Foundation
@testable import photosList

enum MockData {
    static let photosResponse = [photo(id: "52082482833", secret: "87b30b8a02", server: "65535", farm: 66),
                                 photo(id: "52082482833", secret: "87b30b8a02", server: "65535", farm: 66)
    ]
}
