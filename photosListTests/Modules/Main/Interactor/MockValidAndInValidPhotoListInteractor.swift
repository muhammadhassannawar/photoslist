//
//  MockValidAndInValidPhotoListInteractor.swift
//  photosListTests
//
//  Created by Mohamed Hassan Nawar on 18/05/2022.
//

import Foundation
@testable import photosList

class MockValidPhotoListInteractor: PhotoListInteractorProtocol {
    func loadPhotosforKey(searchKey: String, completion: @escaping ([photo]?, String?) -> Void) {
        completion(MockData.photosResponse, nil)
    }
    
    func addPhotosforKey(searchKey: String, photos: [photo]) { }
    
    func isConnectedToNetwork() -> Bool {
        return true
    }
    
    func loadPhotosList(startFrom: Int, searchText: String, completionHundler: @escaping ([photo]?, String?) -> Void) {
        completionHundler(MockData.photosResponse, nil)
    }
}

class MockInValidPhotoListInteractor: PhotoListInteractorProtocol {
    func loadPhotosforKey(searchKey: String, completion: @escaping ([photo]?, String?) -> Void) {
        completion(nil, "No Data")
    }
    
    func addPhotosforKey(searchKey: String, photos: [photo]) { }
    
    func isConnectedToNetwork() -> Bool {
        return true
    }
    
    func loadPhotosList(startFrom: Int, searchText: String, completionHundler: @escaping ([photo]?, String?) -> Void) {
        completionHundler(nil, "No Data")
    }
}
