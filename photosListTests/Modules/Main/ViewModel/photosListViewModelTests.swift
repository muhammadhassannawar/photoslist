//
//  photosListViewModelTests.swift
//  photosListTests
//
//  Created by Mohamed Hassan Nawar on 18/05/2022.
//

import XCTest
@testable import photosList

class photosListViewModelTests: XCTestCase {
    private var sut: PhotosListViewModel!
    private var expectationDesc: String!
    private let dummyPhotos = MockData.photosResponse
    let isDisabled = true
    
    override func setUp() {
        expectationDesc = "PhotosListViewModelTest"
        super.setUp()
    }
    
    override func tearDown() {
        sut = nil
        expectationDesc = nil
        super.tearDown()
    }
    
    func testSUT_whenSearchWithValidCalled_searchTextIsSet_errorMessageEmpty() {
        // Given
        let interactor = MockValidPhotoListInteractor()
        sut = PhotosListViewModel(interactor: interactor)
        
        // When
        sut.didPressSearch(searchText: "Test")
        
        // Then
        XCTAssertEqual(sut.numberOfCells, 2)
        XCTAssertNil(sut.errorMessage)
    }
    
    func testSUT_whenSearchWithInValidCalled_searchTextIsSet_notReloadComponent() {
        // Given
        let interactor = MockInValidPhotoListInteractor()
        sut = PhotosListViewModel(interactor: interactor)
        
        // When
        sut.didPressSearch(searchText: "Test")
        
        // Then
        XCTAssertEqual(sut.reloadComponent, false)
    }
    
    func testSUT_whenLoadPhotosCalledAndSelectCell_PhotoImageUrlSet() {
        // Given
        let interactor = MockValidPhotoListInteractor()
        sut = PhotosListViewModel(interactor: interactor)
        
        // When
        sut.didPressSearch(searchText: "test")
        sut.didSelectRowInPhotosTable(index: 1)
        
        // Then
        XCTAssertEqual(dummyPhotos[1].url, "https://farm66.static.flickr.com/65535/52082482833_87b30b8a02.jpg")
    }
    
    
    func testSUT_whenLoadPhotosCalledAndScrollToLast_loadNextPageAndIncreaseCellCount() {
        // Given
        let interactor = MockValidPhotoListInteractor()
        sut = PhotosListViewModel(interactor: interactor)
        
        // When
        sut.didPressSearch(searchText: "test")
        sut.didScrollToLast()
        
        // Then
        XCTAssertEqual(sut.numberOfCells, 4)
    }
    
    
    func testSUT_whenDidPressSearchBySearchText_TheResultIsSortedAndLoadSortedDataForTheSearchKeyEqualSorted() {
        // Given
        let interactor = MockValidPhotoListInteractor()
        sut = PhotosListViewModel(interactor: interactor)
        
        // When
        sut.didPressSearch(searchText: "test")
        var sortedList: [photo]?
        interactor.loadPhotosforKey(searchKey: "test") { result ,_ in
            sortedList = result
        }
        
        // Then
        XCTAssertEqual(dummyPhotos.count, sortedList?.count)
    }
}

