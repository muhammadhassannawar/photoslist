//
//  MockAPIService.swift
//  photosListTests
//
//  Created by Mohamed Hassan Nawar on 18/05/2022.
//

import XCTest
import Foundation
import Mockit
@testable import photosList

class MockCoreNetwork<Result: Codable>: CoreNetworkProtocol {
    var result: Result?
    var error: Error?
    
    func makeRequest<T: Codable>(request: RequestSpecs<T>,
                                 completion: @escaping (T?, Error?) -> Void) {
        if let result = self.result as? T {
            completion(result, error)
        } else {
            completion(nil, self.error)
            
        }
    }
}
