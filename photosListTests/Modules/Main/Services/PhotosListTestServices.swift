//
//  PhotosListTestServices.swift
//  photosListTests
//
//  Created by Mohamed Hassan Nawar on 19/05/2022.
//


import XCTest
@testable import photosList

class PhotosListTestServices: XCTestCase, Mockable {
    var coreNetwork: MockCoreNetwork<SearchResult>!
    
    override func setUp() {
        coreNetwork = MockCoreNetwork<SearchResult>()
        super.setUp()
    }
    
    override func tearDown() {
        coreNetwork = nil
        super.tearDown()
    }
    
    func testSUT_whenLoadProfileCalled_testInValidResponse() {
        // Given
        let fileName = "LoadPhotos_Invalid"
        var result = ""
        
        // When
        loadJSON(fileName: fileName, type: SearchResult.self) { _, error in
            result = error ?? ""
        }
        // Then
        XCTAssertNotEqual(result, "")
    }
    
    
    func testSUT_whenLoadProfileCalled_testValidResponse() {
        // Given
        let fileName = "LoadPhotos_Valid"
        var result: SearchResult?
        
        // When
        loadJSON(fileName: fileName, type: SearchResult.self) { response, _ in
            result = response
        }
        // Then
        XCTAssertNotNil(result)
    }
}
