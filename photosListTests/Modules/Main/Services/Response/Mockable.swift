//
//  Mockable.swift
//  photosListTests
//
//  Created by Mohamed Hassan Nawar on 19/05/2022.
//

import Foundation

protocol Mockable: AnyObject {
    var bundle: Bundle { get }
    func loadJSON<T: Codable>(fileName: String, type: T.Type, completion: @escaping (T?, String?) -> Void)
}

extension Mockable {
    var bundle: Bundle {
        return Bundle(for: type(of: self))
    }
    
    func loadJSON<T: Codable>(fileName: String, type: T.Type, completion: @escaping (T?, String?) -> Void) {
        guard let path = bundle.url(forResource: fileName, withExtension: "json") else {
            fatalError("Faild to load JSON file.")
        }
        
        do {
            let data = try Data(contentsOf: path)
            let response = try JSONDecoder().decode(T.self, from: data)
            
            return completion(response, nil)
        } catch {
            return completion(nil, error.localizedDescription)
            
        }
    }
}
