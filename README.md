# PhotosList

### Overview

This repo includes a sample project that demonstrates the MVVM patterns on iOS using Combine for binding, use interactor layer and routing layer.

* Xcode 12.5
* Swift 5

in this project The main screen which contain a list of Photos, this list populated by typing search query and this search quary is stored as list of photos to related search quary in the future in case there is no connection you can retrive the photos if you write the same search quary , when click in one of this photos represent image details.

<img src="https://i.postimg.cc/gcZ4CsVx/Simulator-Screen-Shot-i-Phone-13-Pro-Max-2022-05-19-at-17-47-31.png" width="250"> <img src="https://i.postimg.cc/Nft8WcGn/Simulator-Screen-Shot-i-Phone-13-Pro-Max-2022-05-19-at-17-47-36.png" width="250">

# Pods
In this project includes some of useful pods for iOS, such as:
  * pod 'SDWebImage'
  * pod 'Alamofire'
  * pod 'Mockit'

# Included API

This project requires the following API:
* Search for photos by name (https://api.flickr.com/services/rest?method=flickr.photos.search&api_key=&per_page=&format=&nojsoncallback=&page=&text=)
