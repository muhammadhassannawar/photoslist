//
//  NetworkInterface.swift
//  photosList
//
//  Created by Mohamed Hassan Nawar on 17/05/2022.
//

import UIKit
import Alamofire

enum RequestMethod: String {
    case GET, POST, PUT, DELETE
}

enum Encoding {
    case urlEncodedInURL
    case json
}

/// Request specification to collect all related data to perform a request which contain method type, url and parameters
struct RequestSpecs<ResponseType: Decodable> {
    let method: RequestMethod
    let urlString: String
    let parameters: [String: AnyObject]?
    let encoding: Encoding
    init(method: RequestMethod, urlString: String,
         parameters: [String: AnyObject]?, encoding: Encoding = .urlEncodedInURL) {
        self.method = method
        self.urlString = urlString
        self.parameters = parameters
        self.encoding = encoding
    }
}
protocol NetworkingInterface {
    func request<T: Decodable>(_ specs: RequestSpecs<T>,
                               completionBlock: @escaping (T?, Error?) -> Void)
}
