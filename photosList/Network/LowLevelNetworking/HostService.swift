//
//  HostService.swift
//  photosList
//
//  Created by Mohamed Hassan Nawar on 17/05/2022.
//

import UIKit

struct HostService {
    // MARK: - static Properties And Methods
    static func getBaseURL() -> String {
        return "https://api.flickr.com/services/"
    }
    static var headers: [String: String] { [:] }
    static var extraParameters: [String: AnyObject] {
        ["method": "flickr.photos.search",
         "api_key": "39417c145483a7fb3ee91c5fe5bc93fe",
         "format": "json",
         "nojsoncallback": "50",
         "per_page": "20"] as [String: AnyObject]
    }
}
