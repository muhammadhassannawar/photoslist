//
//  CoreNetwork.swift
//  photosList
//
//  Created by Mohamed Hassan Nawar on 17/05/2022.
//

import UIKit

class CoreNetwork {
    // MARK: - Properties
    static var sharedInstance: CoreNetworkProtocol = CoreNetwork()
    fileprivate lazy var networkCommunication: NetworkingInterface = {
        AlamofireAdaptor(baseURL: HostService.getBaseURL(),
                         headers: HostService.headers,
                         extraParameters: HostService.extraParameters)}()
}

// MARK: - Core Network Protocol methods
extension CoreNetwork: CoreNetworkProtocol {
    func makeRequest<T: Codable>(request: RequestSpecs<T>,
                                 completion: @escaping (T?, Error?) -> Void) {
        networkCommunication.request(request, completionBlock: completion)
    }
}

protocol CoreNetworkProtocol {
    func makeRequest<T: Codable>(request: RequestSpecs<T>,
                                 completion: @escaping (T?, Error?) -> Void)
}
