//
//  SearchModel.swift
//  photosList
//
//  Created by Mohamed Hassan Nawar on 17/05/2022.
//

import Foundation

struct SearchResult: Codable {
    var photos: PhotosData
    let stat: String
    var code: Int?
    var message: String?
}

struct PhotosData: Codable {
    let page: Int
    let pages: Int
    let perpage: Int
    let total: Int
    var photo: [photo]
}

struct photo: Codable {
    let id: String
    let secret: String
    let server: String
    let farm: Int
}

extension photo {
    var url: String? {
        return "https://farm\(self.farm).static.flickr.com/\(self.server)/\(self.id)_\(self.secret).jpg"
    }
}
