//
//  SortedPhotos.swift
//  photosList
//
//  Created by Mohamed Hassan Nawar on 17/05/2022.
//

import Foundation

protocol SortedPhotosProtocol {
    func loadSortedPhotosForKey(searchKey: String, completion: @escaping ([photo]?, String?) -> Void)
    func addPhotosWithKeySortedPhotos(searchKey: String, photos: [photo])
}

class SortedPhotos {
    // MARK: - Properties and methods to store, remove, update and check exist of Sorted Suggestions
    public static var shared = SortedPhotos()
    private var storedPhotos: [String: [photo]]?
    
    private func storePhotosForKey(searchKey: String, _ photosForKey: [String: [photo]]) {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(photosForKey) {
            let defaults = UserDefaults.standard
            defaults.set(encoded, forKey: searchKey)
        }
    }
}

extension SortedPhotos: SortedPhotosProtocol {
    func loadSortedPhotosForKey(searchKey: String, completion: @escaping ([photo]?, String?) -> Void) {
        DispatchQueue.global(qos: .utility).async {
            DispatchQueue.main.async {
                if let json = UserDefaults.standard.object(forKey: searchKey) as? Data {
                    let decoder = JSONDecoder()
                    if let loadedData = try? decoder.decode([String: [photo]].self, from: json) {
                        completion(loadedData[searchKey], nil)
                    } else {
                        completion(nil, "Not able to decode")
                    }
                } else {
                    completion(nil, "Not Found in Sorted result and there is no connection")
                }
            }
        }
    }
    
    func addPhotosWithKeySortedPhotos(searchKey: String, photos: [photo]) {
        DispatchQueue.global(qos: .utility).async {
            var photosWithKey = [String: [photo]]()
            photosWithKey.updateValue(photos, forKey: searchKey)
            self.storePhotosForKey(searchKey: searchKey, photosWithKey)
        }
    }
}
