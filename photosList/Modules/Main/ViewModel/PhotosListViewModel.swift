//
//  PhotosListViewModel.swift
//  photosList
//
//  Created by Mohamed Hassan Nawar on 17/05/2022.
//

import Combine
import Foundation

protocol PhotosListViewModelProtocol: ObservableObject {
    func didPressSearch(searchText: String)
    func didScrollToLast()
    func didSelectRowInPhotosTable(index: Int)
    func getCellViewModel( at indexPath: IndexPath ) -> (String, Bool)
}

final class PhotosListViewModel {
    // MARK: - Private Properties
    private let interactor: PhotoListInteractorProtocol
    private let wireFrame: MainWireFrameProtocol?
    private var photosModels: [photo] = []
    private var areThereMorePhotos = true
    private var startFrom = 1
    private var searchText = ""
    var numberOfCells: Int {
        return photosModels.count
    }
    @Published var isLoading = false
    @Published var errorMessage: String?
    @Published var reloadComponent = false
    
    init(interactor: PhotoListInteractorProtocol = PhotoListInteractor(),
         wireFrame: MainWireFrameProtocol? = nil) {
        self.interactor = interactor
        self.wireFrame = wireFrame
    }
    
    // MARK: - Private methods
    private func loadPhotos(searchText: String, startFrom: Int, completion: (()->Void)? = nil) {
        interactor.loadPhotosList(startFrom: startFrom, searchText: searchText) { [weak self] (result, error) in
            completion?()
            if let error = error {
                DispatchQueue.main.async {
                    self?.errorMessage = error
                    self?.reloadComponent = false
                }
            } else if let result = result {
                self?.didLoadPhotos(models: result, startFrom: startFrom)
            }
        }
    }
    
    private func didLoadPhotos(models: [photo], startFrom: Int) {
        if models.isEmpty {
            areThereMorePhotos = false
        }else{
            if startFrom == 1 {
                didFirstTimeLoadPhotos(models: models)
            } else {
                photosModels.append(contentsOf: models)
            }
            reloadComponent = true
            areThereMorePhotos = true
            interactor.addPhotosforKey(searchKey: searchText, photos: photosModels)
        }
    }
    
    private func didFirstTimeLoadPhotos(models: [photo]) {
        guard !models.isEmpty else {
            errorMessage = "No search results for \(searchText)"
            return
        }
        self.photosModels = models
    }
    
    private func setBannerStatus(at index: Int) -> Bool {
        return (index + 1) % 5 == 0 ? false : true
    }
}

// MARK: - Presenter Protocol methods
extension PhotosListViewModel: PhotosListViewModelProtocol {
    func didPressSearch(searchText: String) {
        self.searchText = searchText
        self.photosModels.removeAll()
        self.startFrom = 1
        guard !isLoading else { return }
        isLoading = true
        loadPhotos(searchText: searchText, startFrom: startFrom) { [unowned self] in
            isLoading = false
        }
    }
    
    func didScrollToLast() {
        // Check if no loading recipes request fetch or if we not reach last elelment in search
        guard !isLoading && areThereMorePhotos && interactor.isConnectedToNetwork() else { return }
        self.startFrom = startFrom + 1
        self.isLoading = true
        loadPhotos(searchText: searchText, startFrom: self.startFrom) { [unowned self] in
            isLoading = false
        }
    }
    
    func cancelSearch() {
        self.searchText = ""
        self.photosModels.removeAll()
        reloadComponent = true
    }
    
    func getCellViewModel( at indexPath: IndexPath ) -> (String, Bool) {
        return (photosModels[indexPath.row].url ?? "", setBannerStatus(at: indexPath.row))
    }
    
    func didSelectRowInPhotosTable(index: Int) {
        guard let selectedPhotoUrl = photosModels[index].url else { return }
        wireFrame?.presentPhotoDetails(selectedPhotoUrl)
    }
}
