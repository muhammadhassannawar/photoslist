//
//  MainModule.swift
//  photosList
//
//  Created by Mohamed Hassan Nawar on 17/05/2022.
//

import Foundation

class MainModule {
    func configure(wireframe: MainWireFrame? = nil) -> MainView? {
        guard let viewController = MainView.loadFromNib() else { return nil }
        viewController.viewModel = PhotosListViewModel(wireFrame: wireframe)
        return viewController
    }
}
