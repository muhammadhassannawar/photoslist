//
//  MainWireFrame.swift
//  photosList
//
//  Created by Mohamed Hassan Nawar on 17/05/2022.
//

import UIKit

protocol MainWireFrameProtocol {
    func presentPhotoDetails(_ photo: String)
}

class MainWireFrame: MainWireFrameProtocol {
    var parentNavigationController: UINavigationController?
    
    func presentPhotoDetails(_ photo: String) {
        PhotoDetailsWireFrame().showPhotoDetailsScreen(in: parentNavigationController, photo: photo)
    }
}
