//
//  PhotoListInteractor.swift
//  photosList
//
//  Created by Mohamed Hassan Nawar on 17/05/2022.
//

import Foundation

protocol PhotoListInteractorProtocol {
    func loadPhotosList(startFrom: Int, searchText: String, completionHundler: @escaping ([photo]?, String?) -> Void)
    func loadPhotosforKey(searchKey: String, completion: @escaping ([photo]?, String?) -> Void)
    func addPhotosforKey(searchKey: String, photos: [photo])
    func isConnectedToNetwork() -> Bool
}

final class PhotoListInteractor {
    private let coreNetwork: CoreNetworkProtocol
    private let entity: SortedPhotosProtocol
    private let reachability: ReachabilityProtocol
    
    init(coreNetwork: CoreNetworkProtocol = CoreNetwork(),
         entity: SortedPhotosProtocol = SortedPhotos.shared,
         reachability: ReachabilityProtocol = Reachability()) {
        self.coreNetwork = coreNetwork
        self.entity = entity
        self.reachability = reachability
    }
}
// MARK: - Interactor Protocol methods
extension PhotoListInteractor: PhotoListInteractorProtocol {
    func isConnectedToNetwork() -> Bool {
        reachability.isConnectedToNetwork()
    }
    
    func addPhotosforKey(searchKey: String, photos: [photo]) {
        entity.addPhotosWithKeySortedPhotos(searchKey: searchKey, photos: photos)
    }
    
    func loadPhotosforKey(searchKey: String, completion: @escaping ([photo]?, String?) -> Void) {
        entity.loadSortedPhotosForKey(searchKey: searchKey, completion: completion)
    }
    
    func loadPhotosList(startFrom: Int, searchText: String, completionHundler: @escaping ([photo]?, String?) -> Void) {
        if reachability.isConnectedToNetwork() {
            let parameter = ["text": searchText, "page": startFrom] as [String: AnyObject]
            let request = RequestSpecs<SearchResult>(method: .GET, urlString: "rest", parameters: parameter)
            coreNetwork.makeRequest(request: request, completion: { model, error in
                completionHundler(model?.photos.photo, error?.localizedDescription)
            })
        } else {
            entity.loadSortedPhotosForKey(searchKey: searchText, completion: completionHundler)
        }
    }
}
