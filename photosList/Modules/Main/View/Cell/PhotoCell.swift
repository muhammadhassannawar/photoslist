//
//  PhotoCell.swift
//  photosList
//
//  Created by Mohamed Hassan Nawar on 17/05/2022.
//

import UIKit
import SDWebImage

protocol PhotoCellView {
    func configure(_ url: String, hideBanner: Bool)
}

final class PhotoCell: UITableViewCell {
    // MARK: - Outlets
    @IBOutlet weak var photoImageView: UIImageView!
    @IBOutlet weak var bannerImageView: UIImageView!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        photoImageView.stopLoading()
    }
}

// MARK: - View Protocol
extension PhotoCell: PhotoCellView {
    func configure(_ url: String, hideBanner: Bool) {
        guard let url = URL(string: url) else { return }
        photoImageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        photoImageView.sd_setImage(with: url)
        bannerImageView.isHidden = hideBanner
    }
}
