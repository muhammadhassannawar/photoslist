//
//  MainView.swift
//  photosList
//
//  Created by Mohamed Hassan Nawar on 17/05/2022.
//

import UIKit
import Combine

final class MainView: UIViewController {
    // MARK: - Outlets & Properties
    @IBOutlet weak private var loadingView: UIActivityIndicatorView!
    @IBOutlet weak private var searchTermsField: UISearchBar!
    @IBOutlet weak private var photosTableView: UITableView!{
        didSet {
            setupTableView(photosTableView, PhotoCell.self)
        }
    }
    var viewModel = PhotosListViewModel()
    private var cancellable = Set<AnyCancellable>()
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Photos Search"
        self.setupNavigationBar()
        setupSearchBar()
        binding()
    }
    
    // MARK: - Combine
    private func binding() {
        viewModel.$isLoading
            .sink(receiveValue: { [unowned self] isLoading in
                isLoading ? self.showLoadingView() : self.hideLoadingView()
            }
            )
            .store(in: &cancellable)
        
        viewModel.$reloadComponent
            .sink(receiveValue: { [unowned self] _ in
                self.photosTableView.reloadData()
            }
            )
            .store(in: &cancellable)
        
        viewModel.$errorMessage
            .sink(receiveValue: {[unowned self] errorMessage in
                guard let message = errorMessage, !message.isEmpty else { return }
                self.showMessageAlert(message)
            }
            )
            .store(in: &cancellable)
    }
    
    private func hideLoadingView() {
        self.photosTableView.reloadData()
        loadingView.stopAnimating()
    }
    
    private func showLoadingView() {
        loadingView.startAnimating()
    }
    
    private func cancelSearch() {
        self.searchTermsField.resignFirstResponder()
        self.searchTermsField.text = ""
        viewModel.cancelSearch()
    }
}

// MARK: - UI Search Bar Setup And Delegate
extension MainView: UISearchBarDelegate {
    private func setupSearchBar() {
        searchTermsField.delegate = self
        searchTermsField.barTintColor = UIColor.clear
        if #available(iOS 13.0, *) {
            searchTermsField.searchTextField.textColor = .black
        }
        searchTermsField.backgroundColor = UIColor.clear
        searchTermsField.isTranslucent = true
        searchTermsField.setBackgroundImage(UIImage(), for: .any, barMetrics: .default)
        searchTermsField.setImage(UIImage(), for: .clear, state: .normal)
        searchTermsField.showsCancelButton = true
        let toolbar = UIToolbar()
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                        target: nil, action: nil)
        let doneButton = UIBarButtonItem(title: NSLocalizedString("Cancel", comment: ""), style: .done,
                                         target: self, action: #selector(clickCancelButtonOnKeyboard))
        toolbar.setItems([flexSpace, doneButton], animated: true)
        toolbar.sizeToFit()
        searchTermsField.inputAccessoryView = toolbar
    }
    
    @objc func clickCancelButtonOnKeyboard() {
        cancelSearch()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let text = searchBar.text else { return }
        self.searchTermsField.resignFirstResponder()
        viewModel.didPressSearch(searchText: text)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        cancelSearch()
    }
}

// MARK: - UITableView Data Source And Delegate
extension MainView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfCells
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return getConfiguredPhotoCell(tableView: tableView, for: indexPath)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == (viewModel.numberOfCells) - 1 {
            viewModel.didScrollToLast()
        }
    }
    
    private func getConfiguredPhotoCell(tableView: UITableView, for index: IndexPath) -> UITableViewCell {
        let cellViewModel = viewModel.getCellViewModel(at: index)
        let cell: PhotoCell? = tableView.dequeueReusableCell(for: index)
        cell?.configure(cellViewModel.0, hideBanner: cellViewModel.1)
        cell?.photoImageView.tag = index.row
        setTapRecognizerForPhotoInCell(cell: cell)
        return cell ?? UITableViewCell()
    }
    
    private func setTapRecognizerForPhotoInCell(cell: PhotoCell?) {
        let recognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(gestureRecgonizer:)))
        cell?.photoImageView.addGestureRecognizer(recognizer)
        cell?.photoImageView.isUserInteractionEnabled = true
    }
    
    @objc private func imageTapped(gestureRecgonizer: UITapGestureRecognizer) {
        guard let index = gestureRecgonizer.view?.tag else {
            return
        }
        viewModel.didSelectRowInPhotosTable(index: index)
    }
    
    private func setupTableView(_ tableView: UITableView,_ cellType: UITableViewCell.Type) {
        tableView.delegate = self
        tableView.dataSource = self
        tableView.keyboardDismissMode = .onDrag
        tableView.separatorStyle = .none
        tableView.tableFooterView = UIView()
        tableView.register(cellType)
    }
}
