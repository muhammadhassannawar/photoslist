//
//  PhotoDetailsViewModel.swift
//  photosList
//
//  Created by Mohamed Hassan Nawar on 18/05/2022.
//

import Foundation
import UIKit

protocol PhotoDetailsViewModelProtocol {
    var photo: String { get }
    func didPressDismiss()
}

final class PhotoDetailsViewModel: PhotoDetailsViewModelProtocol {
    // MARK: - Private Properties
    private let wireframe: PhotoDetailsWireFrameProtocol
    var photo: String
    
    init(photo: String,
         wireframe: PhotoDetailsWireFrameProtocol) {
        self.photo = photo
        self.wireframe = wireframe
    }
    
    func didPressDismiss() {
        wireframe.dismissView()
    }
}
