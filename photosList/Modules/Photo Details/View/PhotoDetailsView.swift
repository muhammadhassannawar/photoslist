//
//  PhotoDetailsView.swift
//  photosList
//
//  Created by Mohamed Hassan Nawar on 18/05/2022.
//

import UIKit

class PhotoDetailsView: UIViewController {
    // MARK: - Outlets & Properties
    var viewModel: PhotoDetailsViewModelProtocol?
    @IBOutlet weak var photoImageView: UIImageView!
    
    // MARK: - Lifecycle Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        if let imageUrl = viewModel?.photo {
            photoImageView.sd_setImage(with: URL(string: imageUrl), completed: nil)
        }
    }
    
    @IBAction func dismiss(_ sender: Any) {
        viewModel?.didPressDismiss()
    }
}
