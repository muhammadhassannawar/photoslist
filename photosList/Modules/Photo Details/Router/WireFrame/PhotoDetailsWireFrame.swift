//
//  PhotoDetailsWireFrame.swift
//  photosList
//
//  Created by Mohamed Hassan Nawar on 18/05/2022.
//

import UIKit

protocol PhotoDetailsWireFrameProtocol {
    func dismissView()
    func showPhotoDetailsScreen(in parentNavigationController: UINavigationController?, photo: String)
}

class PhotoDetailsWireFrame: PhotoDetailsWireFrameProtocol {
    var parentNavigationController: UINavigationController?
    func showPhotoDetailsScreen(in parentNavigationController: UINavigationController?, photo: String) {
        guard let parentNavigationController = parentNavigationController ?? self.parentNavigationController,
              let controller = PhotoDetailsModule().configure(parentNavigationController: parentNavigationController, photo: photo) else { return }
        self.parentNavigationController = parentNavigationController
        self.parentNavigationController?.present(controller, animated: true, completion: nil)
    }
    
    func dismissView() {
        self.parentNavigationController?.dismiss(animated: true, completion: nil)
    }
}
