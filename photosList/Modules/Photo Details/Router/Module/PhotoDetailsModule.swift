//
//  PhotoDetailsModule.swift
//  photosList
//
//  Created by Mohamed Hassan Nawar on 18/05/2022.
//

import UIKit

class PhotoDetailsModule {
    func configure(parentNavigationController: UINavigationController? = nil, photo: String) -> PhotoDetailsView? {
        guard let viewController = PhotoDetailsView.loadFromNib() else { return nil }
        let wireframe = PhotoDetailsWireFrame()
        wireframe.parentNavigationController = parentNavigationController
        let viewModel = PhotoDetailsViewModel(photo: photo, wireframe: wireframe)
        viewController.viewModel = viewModel
        return viewController
    }
}
